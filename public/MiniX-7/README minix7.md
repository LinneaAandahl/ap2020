![Screenshot](Screenshot.png)

[link](https://LinneaAandahl.gitlab.io/ap2020/MiniX-7)


For this week miniex, the theme was auto-generator related to a rule-based program. Based on two rules, the program was supposed to run by itself, without any form of direct interactivity. 



We were instructed to start the process by thinking about what kind of rules we wanted to implement in our program. Based on the assigned reading and some examples given to us, this should be an easy task, but I actually found it quite difficult. I had an idea of what I wanted to do with my program, so I just started coding. Along the way I managed to implement two rules to the program: 
	1 - the moving path of circles could never leave the canvas
	2 - every time the moving path hit one of the sides of the canvas, the path would change color. 
Over time, the moving path creates a pattern. This pattern will change if some of the arguments in the code changes. For example the size of the canvas, the speedX or speedY, or the size of the ball itself. This program is a very simple solution to the task we were assigned, but the program still runs by itself, based on some rules.  



I have created a class called Ball, and given the ball some properties like size, moving-speed and color. The moving ball creates the illusion of a path, because I stated the background under function setup(). This means that the background is only drawn once, while the ball, stated in function draw(), is drawn over and over again, creating a «moving path». 

In the class, I also made a function called move(), that uses an if-statement to make the ball move just inside the walls of the canvas. I used the same if-statement to make the moving ball change color when it hit one of the four walls. Well, actually the ball only changes color when it hits the left, right and top wall of the canvas. I tried to make it change color when it hit the bottom wall as well, but I didn’t manage to make this happen. 



The theme auto-generator is connected to autonomy and control. When adding this in a code, the programmer gives in to powerlessness. The program runs automatically on the instructions given to it by the programmer. Although the programmer gives in to powerlessness by allowing the program to run automatically, s/he is still in charge over the program. Anytime s/he wants, the programmer can easily access and change the code. A changing in the code will result in a change in the program. In my program, the moving path of cirles moves and forms a pattern according to a set of rules and arguments I have stated in the code. By changing some of these arguments, the movement and pattern of the path will change. So, even though it is the computer that has control over where the path is moving, I, as the programmer, still have the power to change this. 
Furthermore, I have also used the random function to make the colors change when the ball hits one of the four walls. But, I have set some frames for the random-function in form of the minimum and maximum number to pick a color from. This is because I wanted the colors in the program to be in shades of blue and green. One may say that I have given power to the program, with some kinds of constraints.

