var col = {

  r: 0,
  g: 0,
  b: 0

}

var hair = {
  widht: 280,
  height: 300
}

function setup() {
  createCanvas(1000, 500);
  frameRate(3);

}

function draw() {
  background(255);

//emojino1
  //hair
  noStroke()
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  hair.width = random(250, 400)
  hair.height = random(240, 400)
  arc(300, 275, hair.width, hair.height, QUARTER_PI + HALF_PI, QUARTER_PI, OPEN);

  //head
  fill(255, 204, 0)
  ellipse(300, 300, 200, 260);

  // body
  ellipse (300, 650, 300, 400)
  rect (275, 400, 50, 75)

  //eyes
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  ellipse(265, 255, 35);
  ellipse(335, 255, 35);

  //white in eye
  fill(255, 255, 255);
  ellipse(255, 250, 10);
  ellipse(325, 250, 10);

  //mouth
  fill(255, 102, 102);
  arc(300, 345, 65, 55, 0, PI);

  //nose
  fill(255, 217, 179);
  triangle(285, 325, 300, 290, 315, 325);


//emojino2

  //hair
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  hair.width = random(250, 400)
  hair.height = random(240, 400)
  arc(750, 275, hair.width, hair.height, QUARTER_PI + HALF_PI, QUARTER_PI, OPEN);

  //head
  fill(255, 204, 0)
  ellipse(750, 300, 200, 260);

  //body
  ellipse (750, 650, 300, 400)
  rect (725, 400, 50, 75)

  //eyes
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  ellipse(785, 255, 35)
  ellipse(715, 255, 35)

  //white in eye
  fill(255, 255, 255)
  ellipse(705, 250, 10)
  ellipse(775, 250, 10)

  //mouth
  fill(255, 102, 102);
  arc(750, 345, 65, 55, 0, PI);

  //nose
  fill(255, 217, 179)
  triangle(765, 325, 750, 290, 735, 325);
}
