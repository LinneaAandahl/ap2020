var n = 12, //12 == the numer of lines
      r = 100,//the space between the lines and the circle
      l = 0,//var for the speed of the movement of the lines
      k = 0,//lines
      m = 0,
      t=0;

function setup() {
      createCanvas(800, 800);
}

function draw() {
  background(69, 139, 238);


  strokeWeight(5);
  translate(width / 2, height / 2); //moved the 0,0 point to the middle of the canvas
  rotate(radians(t+=0.3)); //speed of rotation

  fill(250, 225, 0) //color of sun
  ellipse(0,0,150,150) //shape of sun

  //rotation
  for (let i = 0; i < n; i++) {
       let ang = i * 360 / n;
     let x = (r + ((k == i) ? l = 10 * sin(radians(m)) : 10)) *
       cos(radians(ang));
       let y = (r + ((k == i) ? l = 10 * sin(radians(m)) : 10)) * sin(radians(ang));
       push();
       translate(x, y);
       rotate(radians(ang));
       line(0, 0, 75, 0); 
       pop();

    }


  }
