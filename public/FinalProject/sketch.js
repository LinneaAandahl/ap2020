let message = '0';
let expression = {};
let trueEmotion = 'neutral'; //Setting the default facexpression to neutral
let allegedEmotion = 'neutral'; //Setting the default facexpression to neutral

function setup(){
  //Loading the face-API
  faceapi.load
  faceapi.loadSsdMobilenetv1Model('./models'); //Loading models for face detection
  faceapi.loadFaceExpressionModel('./models'); //Loading the models for expression recognition

  //Loading and positioning the images used as backgrounds
  bg2 = createImg("./IMG/facebook.png");
  bg1 = createImg("./IMG/login.jpg"); //This will be displayed when starting the program
  bg2.position(0,0);
  bg1.position(0,0);
  bg2.size(1450,800);
  bg1.size(1450,800);

  //Inputline for name
  inputName = createInput();
  inputName.position(815,237); inputName.size(400,40);
  inputName.attribute("placeholder", "Full name");

  //Inputline for work
  inputWork = createInput();
  inputWork.position(815,295); inputWork.size(400,40);
  inputWork.attribute("placeholder", "Workplace");

  //Inputline for education
  inputEducation = createInput();
  inputEducation.position(815,353); inputEducation.size(400,40);
  inputEducation.attribute("placeholder", "Education");

  //Inputline for city
  inputCity = createInput();
  inputCity.position(815,411); inputCity.size(400,40);
  inputCity.attribute("placeholder", "City");

  //Inputline for relationshipstatus
  inputRelationship = createInput();
  inputRelationship.position(815,470); inputRelationship.size(400,40);
  inputRelationship.attribute("placeholder", "Relationship status");

  //Creating the submit-button
  submit = createButton('Submit');
  submit.position(1140,530); submit.size(75,40);
  submit.mousePressed(profile);

  //Displaying / Hiding video
  video = createCapture(VIDEO); //The main profileprictre
  video.hide();
  postPic = createCapture(VIDEO); //The profilepicture on posts
  postPic.hide();
}

function profile(){ //The profile-page
  //Hiding the BG1 and the DOM-elements related to this (login page)
  bg1.hide();
  inputName.hide();
  inputWork.hide();
  inputEducation.hide();
  inputCity.hide();
  inputRelationship.hide();
  submit.hide();

  //Displaying video as profile picture and positioning it
  video.show();
  video.position(160,31); video.size(265,265);

  //Creating emoji-buttons and positioning them
  button = createImg('./IMG/neutral.png');
  button.size(50,50); button.position(620,375);
  button.mousePressed(neutral);
  button = createImg('./IMG/happy.png');
  button.size(50,50); button.position(685,375);
  button.mousePressed(happy);
  button = createImg('./IMG/sad.png');
  button.size(50,50); button.position(750,375);
  button.mousePressed(sad);
  button = createImg('./IMG/angry.png');
  button.size(50,50); button.position(815,375);
  button.mousePressed(angry);
  button = createImg('./IMG/suprised.png');
  button.size(50,50); button.position(880,375);
  button.mousePressed(surprised);
  button = createImg('./IMG/fearfull.png');
  button.size(50,50); button.position(945,375);
  button.mousePressed(fearfull);
  button = createImg('./IMG/disgusted.png');
  button.size(59,59); button.position(1008,370);
  button.mousePressed(disgusted);

  //Creating the text in the INTRO box, in left side of canvas
  p = createP('Works at');
  p.position(180,400); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#666666');
  p = createP('Education');
  p.position(180,500); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#666666');
  p = createP('Lives in');
  p.position(180,600); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#666666');
  p = createP('Relationship status');
  p.position (180,700); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#666666');
  p = createP('How are you feeling?');
  p.position(690,290); p.style('font-family', 'helvetica'); p.style('color', '#666666');


  //Displaying the data from the inputlines on log-in page in the INTRO box on the profile-page
const name = inputName.value();
  p = createP(name);
  p.position(450,170); p.style('font-size', '25px'); p.style('font-family', 'helvetica'); p.style('color', '#ffffff');
const work = inputWork.value();
  p = createP(work);
  p.position(245,400); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#3c549b');
const education = inputEducation.value();
  p = createP(education);
  p.position(250,500); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#3c549b');
const city = inputCity.value();
  p = createP(city);
  p.position(235,600); p.style('font-size', '15px'); p.style('font-family', 'helvetica');p.style('color', '#3c549b');
const relationship = inputRelationship.value();
  p = createP(relationship);
  p.position(313,700); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#3c549b');
}

  //Async function is used to force face detection to finished before proceeding
async function emotionTracker() {
const myPromise = async () => {
  //Program waits for the function to finish
await faceapi.detectSingleFace(video.elt).withFaceExpressions()
  .then((detectionOfExpression) => {
  //Is there a face in the picture?
if (detectionOfExpression == undefined) {
  console.log("Face not detected");
} else {
  face = detectionOfExpression.detection;
  expression = detectionOfExpression.expressions;
    console.log(expression);
}

  emotionDefiner();
  createPost();
  });
  }
  myPromise();
  }

  //Defining one of the seven emotions mesured on a scale from 0-1
function emotionDefiner() {
  if (expression.neutral > 0.7) {
    trueEmotion = "neutral"
  }
  if (expression.happy > 0.7) {
    trueEmotion = "happy"
  }
  if (expression.sad > 0.7) {
    trueEmotion = "sad"
  }
  if (expression.angry > 0.7) {
    trueEmotion = "angry"
  }
  if (expression.surprised > 0.7) {
    trueEmotion = "surprised"
  }
  if (expression.fearfull > 0.7) {
    trueEmotion = "fearfull"
  }
  if (expression.disgusted > 0.7) {
    trueEmotion = "disgusted"
  }
  console.log("True emotion: " + trueEmotion);
  }

//Calling the different emotions when pressing the emoji-buttons
function neutral() {
  allegedEmotion = "neutral"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function happy() {
  allegedEmotion = "happy"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function sad() {
  allegedEmotion = "sad"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function angry() {
  allegedEmotion = "angry"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function surprised() {
  allegedEmotion = "surprised"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function fearfull() {
  allegedEmotion = "fearfull"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}
function disgusted() {
  allegedEmotion = "disgusted"
  console.log("Alleged emotion: " + allegedEmotion)
  emotionTracker();
}

//Creating post based on the captured emotion
  //If videocapture and chosen emoji match
function createPost() {
  if (allegedEmotion == trueEmotion) {
  if (message != '0') {
    post.hide();
    }
    message = "I'm feeling " + allegedEmotion + " and my feelings have been confirmed."; //The text to be displayed if telling the truth
    post = createP(message);
    post.position(590,640); post.style('font-size', '15px'); post.style('font-family', 'helvetica'); post.style('color', '#666666');
    box = createImg('./IMG/box.png'); //Styling the post
    box.size(568,175); box.position(562,555);
    const name = inputName.value(); //Displaying the name in the post
    p = createP(name);
    p.position(650,570); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#3c549b');
    postPic.show(); //Displaying the profilepicture in the post
    postPic.position(575,565); postPic.size(65,65);
    round = createImg('./IMG/round.png');
    round.position(574,571); round.size(74,52);
  }
  //If videocapture and chosen emoji don't match
  else {
    if (message != '0') {
      post.hide();
    }
      message = "I'm lying! My data has been captured and I'm not feeling " + allegedEmotion + "<br> - I'm feeling " + trueEmotion + "." //The text to be displayed if lying
      post = createP(message);
      post.position(590,635); post.style('font-size', '15px'); post.style('font-family', 'helvetica'); post.style('color', '#666666');
      box = createImg('./IMG/box.png'); //Styling the post
      box.size(568,175); box.position(562,555);
      const name = inputName.value(); //Displaying the name in the post
      p = createP(name);
      p.position(650,570); p.style('font-size', '15px'); p.style('font-family', 'helvetica'); p.style('color', '#3c549b');
      postPic.show();//Displaying the profilepicture in the post
      postPic.position(575,565); postPic.size(65,65);
      round = createImg('./IMG/round.png');
      round.position(574,571); round.size(74,52);
    }
    console.log(message);
    }
