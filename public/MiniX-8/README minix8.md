![Screenshot](Skærmbillede 2020-03-29 kl. 16.06.30.png)

https://herborg.gitlab.io/ap-2020/MiniX8

**Lost In Translation**

*In collaboration with Herborg Hjartvardsdóttir Kjærbæk.*

For this miniX we were asked to work together in pairs, and design a piece of electronic literature. Since both of us comes from a place outside of Denmark, and danish is not our first language, we decided to create a translation program.  Even though all of these countries are quite close, the culture within each country is different, not only when it comes traditions, but also the way we speak and the way we behave. Therefore, we wanted to make a program that may display this.
 
Faroese people are usually very introvert and are in a way afraid to voice their opinion. For instance, if you go to the shops, and the cashier charges 50kr too much, you will look at the receipt and think “it is not that hard to put in the right price”, but often you won’t even say anything to the cashier, because you don’t want to be rude. And in Denmark no one would see an issue in confronting the cashier, and informing them of the error, because it is just a mistake, and everyone can make mistakes.
 
When speaking to someone from another culture, there is so much missed information, because we have certain ways of behaving, and it is not the same in all cultures, therefore your communication might be misinterpreted because of the differences in culture.

When first opening our program, you will see three different flags, respectively the Norwegian, the Danish and the Faroese flag. On top of the Danish flag, there is four words written, “rar”, “underlig”, “sjov” and “ked”. When moving the mouse over one of these four words, the Faroese and Norwegian translation will appear. 

We wanted to make a rollover effect, on the words, so the translated words would disappear again when moving the mouse, but we had a lot of trouble with it. To make the words appear we have used an if-statement, then we tried to use a else-statement (and putting in various things – a different text, an ellipse etc.) along with that to make the words disappear again, but it didn’t work. Then we tried using a while loop:

While(mouseY > 150 && mouseY < 165) {

But this didn’t work either, we had a lot of difficulties getting it to work and we still don’t understand why the while loop isn’t working. Since we couldn’t figure out the solution, we didn’t create the rollover we wanted. 


When talking about code, we often refer to it as a language. Different programs is therefore called different code-languages.
According to the text "The Aesthetics of Generative Code" one can think of code as a language that is only understandable if you know the context of its overall structure, and this is what makes it a language. In the same way as understanding any type of language in the world, one must have some kind of knowledge about how to speak and understand it, but also have some context of the culture, how do people behave in the given country, what is the norm etc.

The text goes on to discuss code as poetry. The authors argue that the "Poetry lies in the meeting of poem and reader, not in the lines of symbols printed on the pages of a book.” (http://www.generativeart.com/on/cic/2000/ADEWARD.HTM) The aesthetic value of code is not only the syntaxes and the written functions, it is also the execution of it. The authors argue that reading someone else's code is much like reading in a foreign language. But in this sense, understanding someone else's code is more that just understanding what the syntaxes or form of the used language. 