![Screenshot](Screenshot.png)

[link](https://LinneaAandahl.gitlab.io/ap2020/MiniX-6)

To this miniexercise we were asked to make a game and implement the class-based object oriented approach. I chose to make a very simple game, that is based on the en
 
The rules of the game are quite simple: catch the apple before it hits the ground.  
By moving the mouse from side to side, the player moves the basket that catches the falling apple. When the apple is caught, it reappears at a random spot on the trees. The apple falls once more, the player have to catch it and this procedure loops until the player are unable to catch the falling apple. If an apple falls on the ground, the screen shifts to a black background, with red letters spelling "game over". If the player wants tp play once more, the page has to be reloaded. 

I used a class-based object to create the falling apple. Inside the class, I created two functions: show() and move(). The show() function is used to create the visuals of the apple like size, color, shape and position. The move() function gives the apple some functionalities like the speed of falling, and also the if-statement that makes it possible for the apple to reappear if it is caught, and the game to finish if the apple falls on the ground. 

Object-oriented programming is an approach to computing that focuses on the interactions between computational objects. the objects in object orientations are grouping of data. The method can be used to make a clear and tidy code for the reader to understand. Even though my class is short and simple, I struggled a lot with creating it. I tried off a lot of methods to try to make the apple move as I wanted. I actually ended up with looking at a similar program online, and managed to locate the syntaxes I needed to make the functionality of the apple. 

I also created four different functions: for the trees, the basket, the static apples and the game over screen. I did it this way to avoid repeating the same code over and over again in the program. The making of the forest and the static apples could probably be done a more effective way than with functions, because one have to repeat some line of code when placing ex. the apples at the trees. 

The idea for this program is based on the first computer games I played when I was young. The games were based on simple rules like: jump over obstacles or catch something before it is broken. So for this miniex, I just wanted to create a game that does not require too much from the player, but at the same time be kind of entertaining. This game could have been improved by f.ex adding a start screen with a start button, keeping track of the score or adding functionality to the arrows on the keyboard to control the basket. 
