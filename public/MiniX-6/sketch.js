let apple;

function setup () {
  createCanvas(700,500);
  frameRate(5);
  apple = new Apple(random(75, 650), random(175, 250), 25);

 }


function draw(){
  background(51, 153, 255);



//green ground
  fill(0, 150,0);
  stroke(0);
  strokeWeight(3);
  rect(0, 450, 700, 50);

  textSize(25);
  fill(0);
  text(`catch the falling apple by moving the mouse`, 100, 475);

//forest
  tree(150, 200, 240);
  tree(75, 200, 175);
  tree(400, 200, 225);
  tree(325, 200, 190);
  tree(550, 200, 190);

  basket();

  staticapples(50, 135, 25);
  staticapples(300, 160, 25);
  staticapples(450, 170, 25);
  staticapples(630, 240, 25);
  staticapples(520, 220, 25);
  staticapples(200, 200, 25);
  staticapples(450, 260, 25);
  staticapples(230, 270, 25);


    apple.show(); //to show the apple constructed in the class
    apple.move(); //to make the apple move


}

function tree (x, y, d){ //static trees in the forest 
  stroke(0);
  strokeWeight(3);
  fill(150, 100, 50)
  rect(x, y, 40, 250)
  fill(0, 160, 0)
  ellipse(x+20, y, d)

}

function basket(){ //basket
  stroke(0);
  strokeWeight(3);
  fill(102, 51, 0);
  rect(mouseX, 400, 70, 50);

}

function staticapples(x, y, d){ //static apples in the trees
  stroke(0);
  strokeWeight(3);
  fill(255, 0, 0);
  ellipse(x, y, d);

}

function gameOver(){ //screen that is shown when player loose
  background(0);
  fill(255, 0, 0);
  textSize(25)
  text (`GAME OVER`, width/2 - 65, height/2);


}

class Apple{
  constructor (x, y, d){
   this.x = x;
   this.y = y;
   this.d = d;

 }

   show(){
     stroke(0);
     strokeWeight(3);
     fill (255, 0, 0)
     ellipse(this.x, this.y, this.d);

   }

  move(){
 this.y = this.y + 10 //speed of falling apple

 if (this.y>height-100 && this.x>mouseX && this.x<mouseX+70){ //if apple is caught by basket, it will reappear at a random place on the trees
   this.x = random(75, 650);
   this.y = random(150, 250);

 }

 if (this.y>height){ //if apple is not caught, gameover
   gameOver();
 }

}
}
