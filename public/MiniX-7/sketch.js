let ball;


function setup() {
  createCanvas(400, 600);
  background(0); //background in setup, so that it is only drawn once

  ball = new Ball();


}

function draw() {

  ball.show(); //to make the ball appear
  ball.move(); //to make the ball move

}

class Ball {

  constructor (x) {
    this.x = 50;
    this.y = height/2;
    this.size = 55;
    this.radius = this.size/2;
    this.speedX = 3; //made the speed of the x- and y-axis different, to make the path form another pattern
    this.speedY = 5;


  }

  show(){ //everything that has something to do with the appearance of the ball

    if(this.x<this.radius || this.x>width-this.radius){
      fill (random(0, 255), 255, random(150, 255))
    }

    if(this.y<this.radius || this.x>height-this.radius){
      fill(random(0, 255), 255, random(150, 255))
    }

   stroke(0);
   strokeWeight(1);
   ellipse(this.x, this.y, this.size, this.size);

 }

  move(){ //everything that has something to do with the moving of the ball
    this.x += this.speedX;
    this.y += this.speedY;

    if(this.x<this.radius || this.x>width-this.radius) {
      this.speedX *= -1;
    }

    if(this.y<this.radius || this.y>height-this.radius) {
      this.speedY *= -1

    }

  }

}
