var col = {

  r: 0,
  g: 0,
  b: 0

}

function setup() {
  createCanvas(1000, 600);
  frameRate(5);

}

function draw() {
  background(0);


  //hair
  noStroke()
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  arc(300, 275, 320, 380, QUARTER_PI + HALF_PI, QUARTER_PI, OPEN);


  //head
  fill(255, 206, 155)
  ellipse(300, 300, 200, 260);

  //bangs
  fill(col.r, col.g, col.b);
  ellipse(250, 173, 130, 70);

  //eyes
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  ellipse(265, 255, 35);
  ellipse(335, 255, 35);

  //white in eye
  fill(255, 255, 255);
  ellipse(255, 250, 10);
  ellipse(325, 250, 10);


  //mouth
  fill(255, 102, 102);
  arc(300, 345, 65, 55, 0, PI);

  //nose
  fill(255, 217, 179);
  triangle(285, 325, 300, 290, 315, 325);

  //boy

  //hair
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  arc(750, 275, 250, 270, QUARTER_PI + HALF_PI, QUARTER_PI, OPEN);


  //head
  fill(255, 206, 155)
  ellipse(750, 300, 200, 260);

  //eyes
  col.r = floor(random(0, 255))
  col.g = floor(random(0, 255))
  col.b = floor(random(0, 255))
  fill(col.r, col.g, col.b)
  ellipse(785, 255, 35)
  ellipse(715, 255, 35)

  //white in eye
  fill(255, 255, 255)
  ellipse(705, 250, 10)
  ellipse(775, 250, 10)

  //mouth
  fill(255, 102, 102);
  arc(750, 345, 65, 55, 0, PI);

  //nose
  fill(255, 217, 179)
  triangle(765, 325, 750, 290, 735, 325);
}
