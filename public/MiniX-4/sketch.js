var spot = {

  x:100,
  y:100
}

var button

function setup() {
  createCanvas(600, 400);
  background (0);

}

function draw() {

   //likebutton
  button = createButton("like");
  button.position(290, 190);
  button.mousePressed(heart);
}

function heart(x, y, size) {
  spot.x = random(0, width);
  spot.y = random(0, height);
  size = 30;
  fill (255, 0, 0, 100)
  noStroke();
  beginShape();
  vertex(spot.x, spot.y);
  bezierVertex(spot.x - size / 2, spot.y - size / 2, spot.x - size, spot.y + size / 3, spot.x, spot.y + size);
  bezierVertex(spot.x + size, spot.y + size / 3, spot.x + size / 2, spot.y - size / 2, spot.x, spot.y);
  endShape(CLOSE);
}
