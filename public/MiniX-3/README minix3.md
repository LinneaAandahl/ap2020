Link til runme: [link](https://LinneaAandahl.gitlab.io/ap2020/MiniX-3)

My throbber is a sun placed on a blue background, with the sunshines rotating around the sun. 
My idea was that my throbber would look like a sun, with something rotating around it. 
I was not sure of how to program the rotation function, so I started the prosess off with a googlesearch. 
Then I found this sketch: https://editor.p5js.org/black/sketches/r15V7RFmz. 
I practically copied the whole program over to my workingsketch, 
and then tried to figure out what the different syntaxes would do. 
Later, I started making changes to the program. Like changes in color, scales and speed of rotation. 

To be honest, I do not quite understand my entire program. 
I managed to figure out that the rotation function has an impact on the way the lines of sunshine rotates around the sun. 

A throbber communicates that something is in process. 
A throbber can show up in many different situations: the loading of a video, 
waiting for an online transaction to complete or the loading of an internet-page. 
For some the icon brings angry and annoyed feelings. 
The throbber makes the flow of information stop, 
and for some this may feel like the throbber is using unnecessarily with time and energy. 