Link til runme: [link](https://LinneaAandahl.gitlab.io/ap2020/MiniX-2)


For this assignment, we were asked to create two emojis. 
My idea was to crate an emoji-girl and an emoji-boy, and then to programme their hair and eyes to change color.

When I was trying to figure out how to create these emojis, I actually started off by google how to programme hair in p5.js. 
I quickly found a programme that created a self-portrait. See link: https://editor.p5js.org/fenfenrita/sketches/ryXtDYXu7. 
I copied a lot of the syntaxes used in my programme from here, and then just changed some of the arguments to make it fit in my canvas. 
Once I had programmed the two emoji-persons, the real challenge presented itself: how can I programme the hair and eyes to change color? 

By watching a lot of Daniel Shiffmans videos on youtube, I felt I now had some idea of how i could solve the problem. 
In the beginning of the programme, I declared my variables: col.r, col.g, col.b. 
When I used these variables later in by programme, the syntaxes vould look like this: 
	col.r = floor(random(0, 255))
	col.g = floor(random(0, 255))
	col.b = floor(random(0, 255))
	fill(col.r, col.g, col.b)
	
The first thing I did, was to add the random-function and gave it some values, 0 <--> 255. 
This means that the number of the color shown in the canvas, would be a random nuber between 0 and 255. 
I also added another syntax, frameRate(). 
This was to make the changing of color slow down, so the reader could more clearly see and experience the different colors. 

I created these two emojis as an attempt to point to the diversity of people in the world. 
Something as simple as changing the color of the eyes and the hair, is enough to represent a lot of people in the world, but yet far from everyone.  
