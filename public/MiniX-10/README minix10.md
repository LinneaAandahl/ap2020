For this weeks mini exercise, we were asked to create three flowcharts. The first one is based on one of our previous mini exercises, while the two others are made as blueprints to visualize my study group´s ideas for our final project. 

My individual flowchart is based on the mini exercise number 6. This week we were supposed to create some kind of game, using a class in our code. I can admit that this may not be my most complicated mini exercise, but this was the exercise I struggled with the most in the programming phase. This is a pretty simple flowchart, where the first three boxes describes the loading of the program. Then a diamond-shape describes the falling apple, that you can choose to catch or not. If you catch the apple, the program loops in the way that the falling apple reappears in the tree and falls once more. If the apple is not caught, the program ends. 

![screenshot](Flowchart minix6.png)

For the two other flowcharts, we were supposed to collaborate with our study groups, and create flowcharts to describe our ideas for our final project. 

Our first idea is called «Do you have corona?». This is a common question today, and we therefor chose to make this program as a sort of test on whether one (allegedly) has corona or not. The user is first asked «are you sick?», and then supposed to answer yes or no. Depending on the users answer, the user is then asked several different questions like «have you had contact with someone who has been in a high risk area?» etc. As one can see in this flowchart, there are three different endings to this program. The ending is chosen, depending on what the users answers on the questions asked. 

![screenshot](flowchart, do you have corona?.png )  (i´m not able to get the sceenshot displayed, but the photo can be found in my repository)


Our second idea is also based on the current situation we're all in nowadays. This program explores the connection between the number of covid-19  cases and the air pollution in a given country. By pressing different buttons, each button representing one of the five countries below, one is presented for two different datas. First, the number of Covid-19 cases, and second, the air pollution. 

![screenshot](flowchart, corona .png)

To create this program, we thought of using two different API´s. One for the number of covid-19 cases and one for the air pollution. Finding the correct API is therefor crucial for our program. We also talked about getting some data of the air pollution from before the breakout of Covid-19. By adding this data, the user of the program will get a clearer view of how much that actually has been changed after the beak out of this virus.



Flowcharts and algorithms can both be seen as a recipe and blueprint as how to solve a given problem. In Taina Buchers text, she describes algorithms as «a step-by-step description» and «a recipe». (kilde). According to Nathan Ensmengers text «the multiple meaning of a flowchart», the flowchart works as a kind of blueprint to the programmer: «Flowcharts are to programmers as blueprints are to engineers». Flowcharts created before the program is written, therefor works as a recipe for the programmer. Both algorithms and flowcharts strive to create a simple overview of the problem, and how to solve it. The flowchart is also used to ease the communication between humans. Often there are several people involved in a programming-project, and by making a flowchart one assures that everyone has the same idea of how the program is supposed to be when it is finished. 

The main difference between the individual and the group flowcharts, is that the individual one is made after the program is finished constructed. The description of flowchart as a blueprint therefor looses some of its meaning. The flowchart is no longer a recipe on how to create the program, rather a visual representation of the finished program. 

